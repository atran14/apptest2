class SalesAgentStatus < ActiveRecord::Base
  has_many :sales_agents
  validates_uniqueness_of :sales_agent_status_name
end
