class JobSource < ActiveRecord::Base
  has_many :jobs
  has_one :sales_agent_status
  validates_uniqueness_of :job_source_name

  def self.search(search, criteria)
    if criteria
      find(:all, :conditions => ["#{criteria[0]} LIKE ?", '%'+search+'%'])
    else
      []
    end
  end

  def search_result
    "#{id} | #{job_source_name} | #{created_at}"
  end

end
