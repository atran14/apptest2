class JobStatusHistory < ActiveRecord::Base
  belongs_to :job, foreign_key: :job_id
  belongs_to :job_status, foreign_key: :job_status_id
  validates_presence_of :job, :job_status

end
