class SalesAgent < ActiveRecord::Base
  has_many :jobs
  belongs_to :sales_agent_status
  before_validation{|record| if record.sales_agent_phone; record.sales_agent_phone.gsub!(/\D/, '') end}
  validates :sales_agent_phone, length: { minimum: 10 }, :unless => Proc.new { |a| a.sales_agent_phone.nil? & a.sales_agent_email?}
  validates :sales_agent_email, format: { with: /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\z/i, }, :unless => Proc.new { |a| a.sales_agent_email.nil? & a.sales_agent_phone? }

  def name_with_initial
    "#{sales_agent_first_name.first}. #{sales_agent_last_name}"
  end

  def full_name
    "#{sales_agent_first_name} #{sales_agent_last_name}"
  end

  def self.search(search, criteria)
    if criteria
      find(:all, :conditions => ["#{criteria[0]} LIKE ?", '%'+search+'%'])
    else
      []
    end
  end

end
