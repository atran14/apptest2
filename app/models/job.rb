class Job < ActiveRecord::Base
  belongs_to :customer
  belongs_to :job_source
  belongs_to :job_type
  belongs_to :sales_agent, foreign_key: :sales_agent_id
  has_one :job_status
  has_many :job_status_histories
  has_many :job_statuses, through: :job_status_histories
  validates_presence_of :customer
  validates_numericality_of :job_price, allow_nil: true

  before_destroy {|record| JobStatusHistory.destroy_all "job_id = #{record.id}"}
  before_create {|record| if record.job_status_id == nil; record.job_status_id = 1 end} #default job_status if blank is Lead
  before_validation {|record| if record.job_status_id == 1; record.internal_job_id = nil elsif record.job_status_id == 2; record.internal_job_id = nil end}
  after_create {|record| record.job_status_histories.new(:job_status_id => record.job_status_id).save! } #create job status history
  after_update {|record| if record.job_status_id != record.job_status_histories.last.job_status_id; record.job_status_histories.new(:job_status_id => record.job_status_id).save! end}

  def self.search(search, criteria)
    if criteria
      find(:all, :conditions => ["#{criteria[0]} LIKE ?", '%'+search+'%'])
    else
      []
    end
  end

  def self.table_limit(limit)
    limit
  end

  def search_result
     "#{id} | #{customer.name_with_initial} | #{created_at}"
  end

end
