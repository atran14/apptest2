class CustomerStatus < ActiveRecord::Base
  has_many :customers
  validates_uniqueness_of :customer_status_name
end
