json.array!(@job_status_lines) do |job_status_line|
  json.extract! job_status_line, :id, :job_status_id, :job_status_date
  json.url job_status_line_url(job_status_line, format: :json)
end
