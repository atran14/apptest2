json.array!(@sales_agent_statuses) do |sales_agent_status|
  json.extract! sales_agent_status, :id, :sales_agent_status_name, :sale_agent_status_date
  json.url sales_agent_status_url(sales_agent_status, format: :json)
end
