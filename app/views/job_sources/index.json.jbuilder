json.array!(@job_sources) do |job_source|
  json.extract! job_source, :id, :job_source_name, :job_source_notes
  json.url job_source_url(job_source, format: :json)
end
