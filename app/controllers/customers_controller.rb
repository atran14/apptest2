class CustomersController < ApplicationController
  before_action :set_customer, only: [:show, :edit, :update, :destroy]


  def leads
    Customer.all.each { |customer| if customer.jobs.any?; if customer.jobs.last.job_status_histories.any?; customer.jobs.last.job_status_histories.last.job_status_id == 1; puts customer.name_with_initial end end }
  end


  # GET /customers
  # GET /customers.json
  def index
    #@customers = Customer.all
  end

  def active
    @active_customers = Customer.limit(params[:limit]).order('updated_at DESC').where(:customer_status_id => 1)
    render layout: false
  end
  def inactive
    @inactive_customers = Customer.limit(params[:limit]).order('updated_at DESC').where(:customer_status_id => 2)
    render layout: false
  end

  # GET /customers/new_lead
  def new_lead
    @customer = Customer.new
  end

  # POST /customers/lead
  # POST /customers/lead.json
  def create_lead

    ActiveRecord::Base.transaction do
      @customer = Customer.new(customer_params)
      if @customer.save
        @job = @customer.jobs.new(job_status_id: 1)
        @job.save!
        end
      end

    respond_to do |format|
      if @customer.save
        format.html { redirect_to @customer, notice: 'Lead (customer) was successfully created.<br>Lead (job) was successfully created.' }
        format.json { render action: 'show', status: :created, location: @customer }
      else
        format.html { render action: 'new_lead' }
        format.json { render json: @customer.errors, status: :unprocessable_entity }
      end
    end
  end

  def search
    @customers = Customer.search params[:search],params[:criteria]
  end

  # GET /customers/1
  # GET /customers/1.json
  def show
    @job = @customer.jobs.new
  end

  # GET /customers/new
  def new
    @customer = Customer.new
  end

  # GET /customers/1/edit
  def edit
  end

  # POST /customers
  # POST /customers.json
  def create
    @customer = Customer.new(customer_params)

    respond_to do |format|
      if @customer.save
        format.html { redirect_to @customer, notice: 'Customer was successfully created.' }
        format.json { render action: 'show', status: :created, location: @customer }
      else
        format.html { render action: 'new' }
        format.json { render json: @customer.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /customers/1
  # PATCH/PUT /customers/1.json
  def update
    respond_to do |format|
      if @customer.update(customer_params)
        format.html { redirect_to @customer, notice: 'Customer was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @customer.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /customers/1
  # DELETE /customers/1.json
  def destroy
    if @customer.customer_status_id == 2
      @customer.update(customer_status_id: 1)
      respond_to do |format|
        format.html { redirect_to customers_url, notice: 'Customer was successfully set active.' }
        format.json { head :no_content }
        end
    else
      @customer.update(customer_status_id: 2)
      respond_to do |format|
        format.html { redirect_to customers_url, notice: 'Customer was successfully set inactive.' }
        format.json { head :no_content }
      end
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_customer
      @customer = Customer.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def customer_params
      params.require(:customer).permit(:customer_first_name, :customer_last_name, :customer_home_address, :customer_home_zip, :customer_home_city, :customer_home_state, :customer_billing_address, :customer_billing_zip, :customer_billing_city, :customer_billing_state, :customer_primary_email, :customer_secondary_email, :customer_primary_phone, :customer_secondary_phone, :customer_notes, :customer_status_id)
    end
end
