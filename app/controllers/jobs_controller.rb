class JobsController < ApplicationController
  before_action :set_job, only: [:show, :edit, :update, :destroy]


  # GET /jobs
  # GET /jobs.json
  def index
  end

  def all
    @jobs = Job.order('id DESC').limit(params[:limit]).all
    render layout: false
  end
  def kitchens
    @jobs_kitchen = Job.order('id DESC').limit(params[:limit]).where(:job_type_id => 2)
    render layout: false
  end
  def bids
    @jobs_bid = Job.order('id DESC').limit(params[:limit]).where(job_status_id:2)
    render layout: false
  end
  def leads
    @jobs_lead = Job.order('id DESC').limit(params[:limit]).where(job_status_id: 1)
    render layout: false
  end
  def bathrooms
    @jobs_bathroom = Job.order('id DESC').limit(params[:limit]).where(:job_type_id => 1)
    render layout: false
  end
  def jobs
    @jobs_job = Job.order('id DESC').limit(params[:limit]).where(job_status_id:3)
    render layout: false
  end

  # GET /jobs/1
  # GET /jobs/1.json
  def show
  end

  def search
    @jobs = Job.search params[:search],params[:criteria]
  end

  # GET /jobs/new
  def new
    @job = Job.new
    @job_status_history = @job.job_status_histories.new
  end

  # GET /jobs/1/edit
  def edit
  end

  # POST /jobs
  # POST /jobs.json
  def create
    @job = Job.new(job_params)

    respond_to do |format|
      if @job.save
        format.html { redirect_to @job, notice: 'Job was successfully created.' }
        format.json { render action: 'show', status: :created, location: @job }
      else
        format.html { render action: 'new' }
        format.json { render json: @job.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /jobs/1
  # PATCH/PUT /jobs/1.json
  def update
    respond_to do |format|
      if @job.update(job_params)
        format.html { redirect_to @job, notice: 'Job was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @job.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /jobs/1
  # DELETE /jobs/1.json
  def destroy
    @job.destroy
    respond_to do |format|
      format.html { redirect_to jobs_url }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_job
      @job = Job.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def job_params
      params.require(:job).permit(:customer_id, :sales_agent_id, :job_source_id, :job_type_id, :internal_job_id, :customer_home_address, :customer_home_city, :customer_home_zip, :customer_home_state, :job_price, :job_notes, :job_status_id)
    end
end
