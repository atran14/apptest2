class SalesAgentStatusesController < ApplicationController
  before_action :set_sales_agent_status, only: [:show, :edit, :update, :destroy]

  # GET /sales_agent_statuses
  # GET /sales_agent_statuses.json
  def index
    @sales_agent_statuses = SalesAgentStatus.all
  end

  # GET /sales_agent_statuses/1
  # GET /sales_agent_statuses/1.json
  def show
  end

  # GET /sales_agent_statuses/new
  def new
    @sales_agent_status = SalesAgentStatus.new
  end

  # GET /sales_agent_statuses/1/edit
  def edit
  end

  # POST /sales_agent_statuses
  # POST /sales_agent_statuses.json
  def create
    @sales_agent_status = SalesAgentStatus.new(sales_agent_status_params)

    respond_to do |format|
      if @sales_agent_status.save
        format.html { redirect_to @sales_agent_status, notice: 'Sales agent status was successfully created.' }
        format.json { render action: 'show', status: :created, location: @sales_agent_status }
      else
        format.html { render action: 'new' }
        format.json { render json: @sales_agent_status.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /sales_agent_statuses/1
  # PATCH/PUT /sales_agent_statuses/1.json
  def update
    respond_to do |format|
      if @sales_agent_status.update(sales_agent_status_params)
        format.html { redirect_to @sales_agent_status, notice: 'Sales agent status was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @sales_agent_status.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /sales_agent_statuses/1
  # DELETE /sales_agent_statuses/1.json
  def destroy
    @sales_agent_status.destroy
    respond_to do |format|
      format.html { redirect_to sales_agent_statuses_url }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_sales_agent_status
      @sales_agent_status = SalesAgentStatus.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def sales_agent_status_params
      params.require(:sales_agent_status).permit(:sale_agent_status_name, :sale_agent_status_date)
    end
end
