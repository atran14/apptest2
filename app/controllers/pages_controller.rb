class PagesController < ApplicationController
  def home
  end

  def about
  end

  def help
  end

  def faq
  end

  def tutorial
  end
end
