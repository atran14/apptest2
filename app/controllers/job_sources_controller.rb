class JobSourcesController < ApplicationController
  before_action :set_job_source, only: [:show, :edit, :update, :destroy]

  # GET /job_sources
  # GET /job_sources.json
  def index
    @job_sources = JobSource.all
  end

  # GET /job_sources/1
  # GET /job_sources/1.json
  def show
  end

  def search
    @job_sources = JobSource.search params[:search],params[:criteria]
  end

  # GET /job_sources/new
  def new
    @job_source = JobSource.new
  end

  # GET /job_sources/1/edit
  def edit
  end

  # POST /job_sources
  # POST /job_sources.json
  def create
    @job_source = JobSource.new(job_source_params)

    respond_to do |format|
      if @job_source.save
        format.html { redirect_to @job_source, notice: 'Job source was successfully created.' }
        format.json { render action: 'show', status: :created, location: @job_source }
      else
        format.html { render action: 'new' }
        format.json { render json: @job_source.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /job_sources/1
  # PATCH/PUT /job_sources/1.json
  def update
    respond_to do |format|
      if @job_source.update(job_source_params)
        format.html { redirect_to @job_source, notice: 'Job source was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @job_source.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /job_sources/1
  # DELETE /job_sources/1.json
  def destroy
    @job_source.destroy
    respond_to do |format|
      format.html { redirect_to job_sources_url }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_job_source
      @job_source = JobSource.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def job_source_params
      params.require(:job_source).permit(:job_source_name, :job_source_notes)
    end
end
