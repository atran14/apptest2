class CorrectSalesAgentStatusNameToSalesAgentStatus < ActiveRecord::Migration
  def up
    rename_column :sales_agent_statuses, :sale_agent_status_name, :sales_agent_status_name
  end

end
