class CreateCustomers < ActiveRecord::Migration
  def change
    create_table :customers do |t|
      t.string :customer_first_name
      t.string :customer_last_name
      t.string :customer_home_address
      t.string :customer_home_state
      t.string :customer_home_zip
      t.string :customer_home_city
      t.string :customer_billing_address
      t.string :customer_billing_zip
      t.string :customer_billing_city
      t.string :customer_billing_state
      t.string :customer_primary_email
      t.string :customer_secondary_email
      t.string :customer_primary_phone
      t.string :customer_secondary_phone
      t.string :customer_notes
      t.integer :customer_status_id

      t.timestamps
    end
  end
end
