class CreateCustomerStatuses < ActiveRecord::Migration
  def change
    create_table :customer_statuses do |t|
      t.string :customer_status_name

      t.timestamps
    end
  end
end
