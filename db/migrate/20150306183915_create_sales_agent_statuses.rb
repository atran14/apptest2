class CreateSalesAgentStatuses < ActiveRecord::Migration
  def change
    create_table :sales_agent_statuses do |t|
      t.string :sale_agent_status_name

      t.timestamps
    end
  end
end
