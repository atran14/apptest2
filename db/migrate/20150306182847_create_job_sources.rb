class CreateJobSources < ActiveRecord::Migration
  def change
    create_table :job_sources do |t|
      t.string :job_source_name
      t.string :job_source_notes

      t.timestamps
    end
  end
end
