class CreateSalesAgents < ActiveRecord::Migration
  def change
    create_table :sales_agents do |t|
      t.string :sales_agent_first_name
      t.string :sales_agent_last_name
      t.integer :sales_agent_phone
      t.decimal :sales_agent_comission
      t.string :sales_agent_email
      t.integer :sales_agent_status_id

      t.timestamps
    end
  end
end
